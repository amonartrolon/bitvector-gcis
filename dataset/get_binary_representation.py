#!/bin/python3
import sys

output_file = open(sys.argv[1] + "_bitmap", "w")
try:
    with open(sys.argv[1], "rb") as f:
        byte = f.read(1)
        while byte != b"":
            bits = [2 if byte[0] & bit > 0 else 1 for bit in (1 << n for n in range(8))]
            converted_byte = "".join(str(x) for x in bits)
            output_file.write(converted_byte)
            byte = f.read(1)
finally:
    output_file.close()
