from pandas import pandas as pd
a = pd.read_csv('compression_ratio.csv', header=None, names=['Nome do Arquivo', 'Tamanho do Arquivo(MB)', 'GCIS', 'Rank V Select MCL', 'Rank V5 Select MCL', 'RRR', 'SD', 'Hybrid', 'Interleaved'], skiprows=1)
df = pd.DataFrame(a)
print(df.to_latex(index_names=False, index=False))
