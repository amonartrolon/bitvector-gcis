import matplotlib.pyplot as plt
import csv
import numpy as np
import os
import sys

with open('results.csv') as infile:
    next(infile)
    reader = csv.reader(infile)
    for line in reader:
        repairsc_bps = [1.0]
        repairsn_bps = [1.0]
        access_gcis_us = []
        access_repairsc_us = []
        access_repairsn_us = []
        filename = line[0]
        filesize_in_bytes = int(line[1])
        filesize_mb = line[29]

        gcis_bps = [float(line[30])]
        print(gcis_bps)
        access_gcis_us = [float(line[6])]
        print(access_gcis_us)

        rank_v_select_mcl_bps = [float(line[31])]
        access_rank_v_select_mcl_us = [float(line[25])]

        rank_v5_select_mcl_bps = [float(line[32])]
        access_rank_v5_select_mcl_us = [float(line[25])]

        rrr_bps = [float(line[33])]
        access_rrr_us = [float(line[26])]

        sd_bps = [float(line[34])]
        access_sd_us = [float(line[27])]

        il_bps = [float(line[36])]
        access_il_us = [float(line[28])]

        """
        for i in range(4,153,2):
            repairsc_bps.append(float(line[i]))
            access_repairsc_us.append(float(line[i+1]))
        points = zip(repairsc_bps, access_repairsc_us)
        points = dominant_points(points)
        repairsc_bps = [x for x,_ in points]
        access_repairsc_us = [x for _,x in points]

        print(access_repairsc_us[-1],flush=True)
        for i in range(152,len(line),2):
            repairsn_bps.append(float(line[i]))
            access_repairsn_us.append(float(line[i+1]))

        points = zip(repairsn_bps,access_repairsn_us)
        points = dominant_points(points)
        repairsn_bps = [x for x,_ in points]
        access_repairsn_us = [x for _,x in points]

        print(access_repairsn_us[-1],flush=True)
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.grid(True)
        # print("bps = ",gcis_bps[i],repairsc_bps[i],repairsn_bps[i])
        # print("access = ",access_gcis_us[i],access_repairsc_us[i],access_repairsn_us[i])
        ax.set_title(filename + ' (' + "{:.2f}".format(float(filesize_mb))+'MB)')
        t_x = ax.set_xlabel("Taxa de Compressão (%)")
        t_x = ax.set_ylabel("Tempo (s)")
        plt.semilogy(gcis_bps,access_gcis_us,label='GCIS',color='green',marker='o',markersize=3,linestyle='None')
        plt.semilogy(rank_v_select_mcl_bps,access_rank_v_select_mcl_us,label='Rank V MCL',color='red',marker='v',markersize=3, linestyle='None')
        plt.semilogy(rank_v5_select_mcl_bps,access_rank_v5_select_mcl_us,label='Rank V5 MCL',color='gold',marker='s',markersize=3, linestyle='None')
        plt.semilogy(rrr_bps,access_rrr_us,label='RRR',color='blue',marker='p',markersize=3, linestyle='None')
        plt.semilogy(sd_bps,access_sd_us,label='SD',color='black',marker='P',markersize=3, linestyle='None')
        plt.semilogy(il_bps,access_il_us,label='Interleaved',color='yellow',marker='d',markersize=3, linestyle='None')
        max_bps = max([max(rrr_bps), max(rank_v_select_mcl_bps), max(rank_v5_select_mcl_bps), max(sd_bps), max(il_bps)])
        print("max bps = ",max_bps) 
        upper = float(max_bps) * 1.1
        step = 0.1*float(max_bps)
        # ax.set_xticks(np.arange(0,upper,step))
        # ax.set_yticks(np.arange(1,300,50))
        ax.set_ylim(0.0,-10)
        ax.set_xlim(0,max_bps + 10)
        # max_bps = float(max(gcis_bps[i],repair_bps[i]))
        # ax.set_xlim(0,max_bps+0.1)
        ax.legend(loc='best')
        # plt.show()
        fig.savefig(filename+'_select.pdf',dpi=600,format='pdf')
