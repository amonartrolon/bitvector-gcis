from pandas import pandas as pd
a = pd.read_csv('compression_time.csv', header=None, names=['Nome do Arquivo', 'GCIS', 'RRR', 'SD', 'Hybrid', 'Interleaved'], skiprows=1)
df = pd.DataFrame(a)
print(df.to_latex(index_names=False, index=False))
