#!/bin/python3

import sys

file_name = sys.argv[1]

f = open(file_name, "r+")

lines = f.read()
lines = lines.replace('1', '0')
lines = lines.replace('2', '1')
f.seek(0)

f.write(lines)

print(lines)
