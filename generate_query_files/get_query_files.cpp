#include <fstream>
#include <random>

int main(int argc, char **argv)
{
  if (argc != 2) {
    return -1;
  }

  std::string filename_prefix(argv[1]);
  std::ifstream f(filename_prefix, std::ios::binary| std::ios::ate);
  std::ofstream output_access_file(filename_prefix + "_access", std::ios::out);
  std::ofstream output_select_file(filename_prefix + "_select", std::ios::out);
  std::ofstream output_rank_file(filename_prefix + "_rank", std::ios::out);


  char c;
  unsigned long int size = f.tellg();

  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<int> position_dist(0,size - 1);
  std::uniform_int_distribution<int> bit_value_dist(0, 1);
  unsigned long int i;
  for (i = 0; i < 1000000; ++i) {
    output_access_file << position_dist(rng) << std::endl;
  }

  for (i = 0; i < 1000000; ++i) {
    output_rank_file << position_dist(rng) << " " << bit_value_dist(rng) << std::endl;
  }

  for (i = 0; i < 1000000; ++i) {
    output_select_file << position_dist(rng) << " " << bit_value_dist(rng) << std::endl;
  }


  return 0;
}
