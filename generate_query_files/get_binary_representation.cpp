#include <fstream>
#include <random>

int main(int argc, char **argv)
{
  if (argc != 2) {
    return -1;
  }

  std::string filename_prefix(argv[1]);
  std::ifstream f(filename_prefix, std::ios::binary| std::ios::in);
  std::ofstream output_bitmap_file(filename_prefix + "_bitmap", std::ios::out);
  std::ofstream output_access_file(filename_prefix + "_access", std::ios::out);
  std::ofstream output_select_file(filename_prefix + "_select", std::ios::out);
  std::ofstream output_rank_file(filename_prefix + "_rank", std::ios::out);


  char c;
  unsigned long int size = 0;
  unsigned long int amount_of_ones = 0;
  while (f.get(c))
  {
    for (int i = 7; i >= 0; i--)
    {
      char to_write = ((c >> i) & 1) > 0 ? '2': '1';
      if (to_write == '2') {
        ++amount_of_ones;
      }
      output_bitmap_file << to_write;
      ++size;
    }
  }
  output_bitmap_file << std::endl;

  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<int> position_dist(0,size - 1);
  std::uniform_int_distribution<int> ones_dist(0,amount_of_ones - 1);
  std::uniform_int_distribution<int> bit_value_dist(0, 1);
  unsigned long int i;
  for (i = 0; i < 1000000; ++i) {
    output_access_file << position_dist(rng) << std::endl;
  }

  for (i = 0; i < 1000000; ++i) {
    output_rank_file << position_dist(rng) << " " << bit_value_dist(rng) << std::endl;
  }

  for (i = 0; i < 1000000; ++i) {
    output_select_file << ones_dist(rng) << " " << bit_value_dist(rng) << std::endl;
  }


  return 0;
}
